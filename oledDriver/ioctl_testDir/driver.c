#include <stdio.h>
#include <sys/types.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/ioctl.h>
#include <stdlib.h>
 
#include "driver.h"

int main ( int argc, char *argv[] )
{
	struct ioctl_mesg msg;
	const char *ioctl_command;
	char command;
	int fd;

	char * pEnd1;
	char * pEnd2;

    	if ( argc != 4 ) {
        	printf( "Usage: %s ([1]command) ([2]string to be printed) ([3]line number) \n\n", argv[0] );
		return -1;
    	}

	//  ************ argument setting ******************************************************
	ioctl_command 	= argv[1];
	printf("ioctl command debug: %c \n", *ioctl_command);

	memset(  msg.kbuf, '\0', sizeof(char) * MAX_BUF_LENGTH );
	strncpy( msg.kbuf, argv[2],  MAX_BUF_LENGTH);
	msg.kbuf[MAX_BUF_LENGTH-1] = '\0';  // add null terminator to prevent buffer overflow

	msg.lineNumber   = (unsigned int) strtoul(argv[3],&pEnd1,10);
	//msg.nthCharacter = (unsigned int) strtoul(argv[4],&pEnd2,10);

	//****************************************************************************************   

	fd = open("/dev/koled", O_WRONLY | O_NDELAY);
	if(fd < 0){
		printf("[User level Debug] ERR: Unable to open koled \n");
		return -1;
	}

	command = *(ioctl_command);
	
	switch( command ){
		// clear the LCD display
		case (IOCTL_CLEAR_DISPLAY ):
			printf("Koled IOCTL Option: Clear Display \n");	

			if( ioctl( fd, (unsigned int) IOCTL_CLEAR_DISPLAY, &msg) < 0)
				perror("[ERROR] IOCTL_CLEAR_DISPLAY \n");
			break;

		// print on the beginning of the first line of the LCD. Other arguments ignored
		case (IOCTL_PRINT_ON_FIRSTLINE ):
			printf("Koled IOCTL Option: Print on First Line \n");	

			if( ioctl( fd, (unsigned int) IOCTL_PRINT_ON_FIRSTLINE, &msg) < 0)
				perror("[ERROR] IOCTL_PRINT_ON_FIRSTLINE \n");			
			break;

		// print on the beginning of the second line of the LCD. Other arguments ignored
		case (IOCTL_PRINT_ON_SECONDLINE ):
			printf("Koled IOCTL Option: Print on Second Line \n");	

			if( ioctl( fd, (unsigned int) IOCTL_PRINT_ON_SECONDLINE, &msg) < 0)
				perror("[ERROR] IOCTL_PRINT_ON_SECONDLINE \n");			
			break;		

		// print on the specified position (line number, nth Character) of the LCD. 
		case (IOCTL_PRINT_ON_THIRDLINE ):
			printf("Koled IOCTL Option: IOCTL_PRINT_ON_THIRDLINE \n");	

			if( ioctl( fd, (unsigned int) IOCTL_PRINT_ON_THIRDLINE, &msg) < 0)
				perror("[ERROR] IOCTL_PRINT_ON_THIRDLINE \n");				
			break;

		// enable a blinking cursor on the LCD
		case (IOCTL_PRINT_ON_FOURTHLINE ):
			printf("Koled IOCTL Option: IOCTL_PRINT_ON_FOURTHLINE \n");

			if( ioctl( fd, (unsigned int) IOCTL_PRINT_ON_FOURTHLINE, &msg) < 0)
				perror("[ERROR] IOCTL_PRINT_ON_FOURTHLINE \n");
			break; 
		
		// disable a blinkin cursor on the LCD
		case (IOCTL_RESERVED ):
			printf("Koled IOCTL Option: RESERVED \n");

			if( ioctl( fd, (unsigned int) IOCTL_RESERVED, &msg) < 0)
				perror("[ERROR] IOCTL_RESERVED \n");
			break;
		
		case (WRITE):
			write(fd, msg.kbuf, sizeof(char) * MAX_BUF_LENGTH );
			break;

		default:
			printf("[User level Debug] KOLED Driver (ioctl): No such command \n");
			break;
	}

	close(fd);	
	printf("KOLED User level Test Program \n");
}

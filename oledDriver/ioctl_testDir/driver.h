#ifndef DRIVER_H_
#define DRIVER_H_

#include <linux/ioctl.h>

#define MAX_BUF_LENGTH  	50  /* maximum length of a buffer to copy from user space to kernel space
				       (MUST NOT CHANGE THIS)
				    */
struct ioctl_mesg{
	char kbuf[MAX_BUF_LENGTH];	// a string to be printed on the LCD

	unsigned int lineNumber;	// line number (should be either 1 or 2)
	unsigned int nthCharacter;	// nth Character of a line (0 refers to the beginning of the line)
};

// ******************* IOCTL COMMAND ARGUMENTS ********************************************

#define KOLED_MAGIC_NUMBER   		0xBC  // a "magic" number to uniquely identify the device 

#define IOCTL_CLEAR_DISPLAY 	  	'0'   // Identifiers for ioctl reqursts
#define IOCTL_PRINT_ON_FIRSTLINE  	'1'
#define IOCTL_PRINT_ON_SECONDLINE 	'2'	/* (Note) ioctl will not be called if this is unsigned int 2, which
						          is a reserved number. Thus it is fixed to '2'
						 */
#define IOCTL_PRINT_ON_THIRDLINE 	'3'
#define IOCTL_PRINT_ON_FOURTHLINE	'4'
#define IOCTL_RESERVED				'5'

#define WRITE						'W'   

// ******************** IOCTL MACROS ********************************************************

#define KOLED_IOCTL_CLEAR_DISPLAY    	_IOW( KOLED_MAGIC_NUMBER, IOCTL_CLEAR_DISPLAY , struct ioctl_mesg)
#define KOLED_IOCTL_PRINT_ON_FIRSTLINE   _IOW( KOLED_MAGIC_NUMBER, IOCTL_PRINT_ON_FIRSTLINE,  struct ioctl_mesg)
#define KOLED_IOCTL_PRINT_ON_SECONDLINE  _IOW( KOLED_MAGIC_NUMBER, IOCTL_PRINT_ON_SECONDLINE, struct ioctl_mesg)
#define KOLED_IOCTL_PRINT_ON_THIRDLINE  _IOW( KOLED_MAGIC_NUMBER, IOCTL_PRINT_ON_THIRDLINE, struct ioctl_mesg)

#define KOLED_IOCTL_PRINT_ON_FOURTHLINE	_IOW( KOLED_MAGIC_NUMBER, IOCTL_PRINT_ON_FOURTHLINE struct ioctl_mesg)
#define KOLED_IOCTL_RESERVED 		_IOW( KOLED_MAGIC_NUMBER, IOCTL_RESERVED, struct ioctl_mesg)

#endif

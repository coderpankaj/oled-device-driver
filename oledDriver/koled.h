#ifndef KOLED_H_
#define KOLED_H_

// ********* Linux driver Constants ********************

#define MINOR_NUM_START		0   // minor number starts from 0
#define MINOR_NUM_COUNT		1   // the number of minor numbers required

#define MAX_BUF_LENGTH  	50  // maximum length of a buffer to copy from user space to kernel space

#define MIN(x, y) (((x) < (y)) ? (x) : (y))


// ********* IOCTL COMMAND ARGUMENTS *******************

#define IOCTL_CLEAR_DISPLAY 	  	'0'	// Identifiers for ioctl reqursts
#define IOCTL_PRINT_ON_FIRSTLINE  	'1'
#define IOCTL_PRINT_ON_SECONDLINE 	'2'	/* (Note) ioctl will not be called if this is unsigned int 2, which
						          is a reserved number. Thus it is fixed to '2'
						 */
#define IOCTL_PRINT_ON_THIRDLINE 	'3'
#define IOCTL_PRINT_ON_FOURTHLINE	'4'
#define IOCTL_RESERVED			'5'

struct ioctl_mesg{				// a structure to be passed to ioctl argument
	char kbuf[MAX_BUF_LENGTH];

	unsigned int lineNumber;
	unsigned int nthCharacter;
};


// ********* Device Structures *********************************************************************

#define CLASS_NAME  	"koled"
#define DEVICE_NAME 	"koled"

static dev_t 		dev_number;	// dynamically allocated device major number
struct cdev  		koled_cdev;	// cdev structure
static struct class *  	koled_class;	// class structure

// ********* GPIO Support *************************************************************************

typedef enum pin_dir
{
	INPUT_PIN  = 0,
	OUTPUT_PIN = 1
} PIN_DIRECTION;

#endif
